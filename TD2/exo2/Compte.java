package encapsulation;
import java.util.Scanner;


public class Compte {
	
	String nom,prenom;
	int num_compte,solde;
	
	Compte(String nom, String prenom, int num, int solde){
		this.nom = nom;
		this.prenom = prenom;
		this.num_compte = num;
		this.solde = solde;
		}

	
	public int getNum_compte() {
		return num_compte;
	}

	public int getSolde() {
		return solde;
	}

	public void depot(int s) {
		this.solde = this.solde + s;
	}
	boolean retrait(int r) {
		if (this.solde < r) {
			return false;
		}
		else {
			this.solde = this.solde - r;
			return true;
		}
	}
	
	public void menu() {
		System.out.println("\n1- Information sur le compte\n2- Solde\n3- Depot\n4- Retrait\n5- Quitter");
		System.out.println("Choix : ");
		Scanner input = new Scanner(System.in);
		int a = input.nextInt();
		switch(a) {
		case 1:
			System.out.println("Nom : "+this.nom+"\nPr�nom : "+this.prenom+"\nNum�ro de Compte : "+this.num_compte);
			menu();
			break;
		case 2:
			System.out.println("Solde du client : " + this.solde);
			menu();
			break;
		case 3:
			System.out.println("Entrez le d�p�t que vous voulez effectuer : ");
			int depot = input.nextInt();
			this.solde = this.solde + depot;
			System.out.println("D�p�t �ffectu�");
			menu();
			break;
		case 4:
			System.out.println("Entrez la valeur du retrait � effectuer : ");
			int retrait = input.nextInt();
			if (retrait(retrait) == false) {
				System.out.println("Fond insuffissant !");
			}
				else {
					System.out.println("Retrait r�ussis !");
				}
			menu();
			break;
		case 5:
			System.out.println("Au revoir !");
			break;
		}
	}
}
