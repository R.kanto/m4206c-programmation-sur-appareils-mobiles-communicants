package structure;

public class Personne {

	public String nom,prenom;
	public int annee;
	
	//Constructeur
	Personne(String nom, String prenom, int annee){
		this.nom = nom;
		this.prenom = prenom;
		this.annee = annee;
	}
	
	//Methodes
	String getNom() {
		return this.nom;
	}
	
	String getPrenom() {
		return this.prenom;
	}
	
	int getAnnee() {
		return this.annee;
	}
	
	int getAge(int a) {
		int age = a - this.annee;
		return age;
	}
	
	void setAnnee(int a) {
		this.annee = this.annee+a;
	}
	
	void affiche() {
		System.out.println("Nom : " + nom + "\nPr�nom : " + prenom + "\nN�e en : " + annee);
	}
	
}
